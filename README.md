# cloudunstor 
checks if a given triplestore already exists in a given volume, if not, gets a triplestore from cloudstor and puts it in a docker volume then exits.

|  env | example  |
|---|---|
| $TRIPLESTORE  |  tuia |
| $FILE_URI | https://cloudstor.aarnet.edu.au/plus/public.php/webdav/  |
| $FILE_USER  | RQTAyLuCDWFQIM  |
| $FILE_PASS  | fPCnaqwYWLIRjJi |

```
docker volume inspect $TRIPLESTORE
docker volume rm $TRIPLESTORE
#
docker volume create $TRIPLESTORE
docker pull registry.gitlab.com/kimihia-te-matangaro/cloudunstor
docker run -it -e "TDB2=$FILE_URI" -e "U=$FILE_USER:$FILE_PASS" --rm -v $TRIPLESTORE:/data registry.gitlab.com/kimihia-te-matangaro/cloudunstor
```

