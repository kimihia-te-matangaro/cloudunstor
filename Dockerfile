FROM debian
WORKDIR /root
RUN apt-get -q update && apt-get install -qy \
    curl \
    nano
COPY . .
VOLUME "/data"
CMD ./run.sh
